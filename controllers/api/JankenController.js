const JankenModel = require("../../models/api/JankenModel");
const jankenModel = new JankenModel();

class JankenController {
  getResultSuit(req, res) {
    let { playerChoice, comChoice } = req.query;

    playerChoice = jankenModel.getRankJanken(playerChoice);
    comChoice = jankenModel.getRankJanken(comChoice);

    const result = {
      winner: "",
      message: "",
      hasWinner: true,
    };

    if (playerChoice === comChoice) {
      result.message = "DRAW";
      result.hasWinner = false;
    } else if (
      playerChoice - comChoice === 1 ||
      playerChoice - comChoice === -2
    ) {
      result.message = "PLAYER 1 WIN";
      result.winner = "player";
    } else {
      result.message = "COM WIN";
      result.winner = "com";
    }

    res.json(result);
  }
}
module.exports = JankenController;
