const UserModel = require("../models/UserModel");
const userModel = new UserModel();
const helperLoginState = require("../helpers/login");

class Authentication {
  constructor() {}

  checkLogin(req, res, next) {
    if (!helperLoginState.isLogin) {
      res.redirect("login");
    } else {
      next();
    }
  }

  doLogin(req, res) {
    const params = req.body;
    const validating = userModel.getUserByEmailAndPassword(params);
    const out = {
      message: "Users not found",
      data: {},
    };
    if (validating) {
      out.message = "Success login";
      out.data = validating.data;
      helperLoginState.isLogin = true;
    }
    res.redirect("game");
  }
}

module.exports = Authentication;
