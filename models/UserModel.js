const config = require("../helpers/config");
const Model = require("./Model");
class UserModel extends Model {
  constructor() {
    super();
    this.database = config.read_database();
  }

  getUserByEmailAndPassword(params) {
    const { email, password } = params;
    const users = super.getAllData();
    let out = false;
    users.forEach((user) => {
      if (user.email == email && user.password == password) {
        out = { data: { ...user } };
      }
    });
    return out;
  }
}

module.exports = UserModel;
