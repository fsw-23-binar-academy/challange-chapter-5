const Model = require("../Model");
const config = require("../../helpers/config");
class JankenModel extends Model {
  constructor() {
    super();
    this.database = config.read_database("./db/rankJanken.json");
  }

  getRankJanken(choice) {
    const ranks = this.getAllData();
    const data = ranks.filter((r) => r.item == choice);
    if (!data) {
      console.log("User choice not found");
    }

    return data[0].rank;
  }
}

module.exports = JankenModel;
