var express = require("express");
var router = express.Router();
const JankenController = require("../controllers/api/JankenController");

const jankenController = new JankenController();

/* GET janken list item. */

router.get("/suit", jankenController.getResultSuit);

module.exports = router;
