const express = require("express");
const cors = require("cors");
const app = express();
const Authentication = require("./controllers/Authentication");
const authController = new Authentication();
const apiRoute = require("./routes/api");

app.use(cors());

var bodyParser = require("body-parser");
app.set("view engine", "ejs");

// parse application/json
app.use(bodyParser.urlencoded({ extended: false }));

//untuk load assets nya
app.use(express.static("public"));

// midleware ketika akses halaman game
app.use("/game", authController.checkLogin);

app.use("/api", apiRoute);

// ROUTING PAGE
app.get("/", (req, res) => {
  res.render("landing");
});

app.get("/game", (req, res) => {
  res.render("game");
});

app.get("/login", (req, res) => {
  res.render("login");
});

app.post("/login", authController.doLogin);

app.listen(3000, () => {
  console.log("listening on port 3000");
});
