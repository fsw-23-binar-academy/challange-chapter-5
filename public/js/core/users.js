class UserJanken extends Janken {
  constructor(props) {
    super(props);
    this.userType = props.type;
    this.userName = props.name;
    this.listUserType = ["com", "player"];
    this.userChoice = "";
  }

  #setUserChoice(choice) {
    this.userChoice = choice;
  }

  changeUserChoice(newChoice) {
    //enkapsulasi -> cek dulu apakah data yang mau dirubah sesuai dengan ketentuan
    if (this.listItemJanken.includes(newChoice)) {
      this.#setUserChoice(newChoice);
    }
  }

  getUserChoice() {
    return this.userChoice;
  }

  #setTypeUser(type) {
    this.userType = type;
  }

  changeUserType(newType) {
    if (this.listUserType.includes(newType)) {
      this.#setTypeUser(newType);
    }
  }

  getTypeUser() {
    return this.userType;
  }

  setUserName(name) {
    this.userName = name;
  }

  getUserName() {
    return this.userName;
  }

  choose() {
    super.startGame();
    if (this.isPlayed) {
    }
  }

  restartGame() {
    super.restartGame();
    this.#setUserChoice("");
  }
}
