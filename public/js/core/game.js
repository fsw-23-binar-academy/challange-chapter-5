class Game {
  constructor() {
    if (this.constructor === Game) {
      //abstract class cannot be instantiated
      throw new Error("Cannot instantiate from Abstract Class");
    }
    this.isPlayed = false;
  }

  startGame() {
    this.isPlayed = true;
    console.log("Game Played");
    console.log("isPlayed", this.isPlayed);
  }

  restartGame() {
    this.isPlayed = false;
    console.log("Game Restarted");
    console.log("isPlayed", this.isPlayed);
  }

  resultGame() {
    console.log("Game Result");
  }

  finishGame() {
    this.isPlayed = false;
    console.log("Game Finish");
    console.log("isPlayed", this.isPlayed);
  }
}

const GamePvp = (Base) =>
  class extends Base {
    findEnemyPlayer() {
      console.log("Finding other player");
    }
  };
// Military Module/Helper
const GamePvc = (Base) =>
  class extends Base {
    selectLevelGame() {
      console.log("Selected level like hard, easy, medium");
    }
  };
