class ComputerUsers extends UserJanken {
  constructor(props) {
    super(props);
  }

  choose() {
    if (!this.isPlayed) {
      this.userChoice =
        this.listItemJanken[
          Math.floor(Math.random() * this.listItemJanken.length)
        ];
      this.setItemJankenActive(this.getTypeUser(), this.userChoice);
    }
    super.choose();
  }
}
