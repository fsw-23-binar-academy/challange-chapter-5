//menggunakan get element by id
const loadingScreen = document.getElementById("loading-screen");
//menggunakan query selector # = id, . = class, [ = attribute,
const gameScreen = document.querySelector("#game-screen");

hideLoading = () => {
  loadingScreen.style.display = "none";
};

showLoading = () => {
  loadingScreen.style.display = "flex";
};

showGameScreen = () => {
  gameScreen.style.display = "block";
};

const playerOne = new PlayerUsers({
  name: "Player 1",
  type: "player",
});
const comUser = new ComputerUsers({
  name: "Computer",
  type: "com",
});

playerChoose = (itemJankenChoosed) => {
  if (!playerOne.isPlayed) {
    playerOne.choose(itemJankenChoosed);
    comUser.choose();
    const dataSuit = playerOne.suit(
      playerOne.getUserChoice(),
      comUser.getUserChoice(),
      playerOne
    );
  } else {
    alert(
      "You have start this game, to restart click refresh button at bottom screen"
    );
  }
};

restartGame = () => {
  playerOne.restartGame();
  comUser.restartGame();
};

const userNamePlayer = document.querySelector("#username-player");
const userNameComputer = document.querySelector("#username-computer");

userNamePlayer.innerText = playerOne.getUserName();
userNameComputer.innerText = comUser.getUserName();

// Show page
setTimeout(function () {
  hideLoading();
  showGameScreen();
}, 3000);
