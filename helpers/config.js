const fs = require("fs");
const PATH_DB = "./db/users.json";

const config = {
  read_database: (db = PATH_DB) => {
    return fs.readFileSync(db, "utf-8");
  },
  replace_database: (data) => {
    fs.writeFile(PATH_DB, JSON.stringify(data), (err) => {
      if (err) throw err;
      return true;
    });
  },
};

module.exports = config;
